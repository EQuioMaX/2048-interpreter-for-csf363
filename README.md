# 2048 Interpreter for CS F363
A simple interpreter for a variant of the 2048 game written in Rust.
Redirect `stderr` to a file for proper formatting of `stdout`.

This project is available on [GitLab](https://gitlab.com/EQuioMaX/2048-interpreter-for-csf363)

## Prerequisites

* Rust: 
```sh
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
* Set up `stable` toolchain:

```sh
rustup default stable
rustup update
```
* Or, using `make`:
```sh
make deps_install
```

## Build

* In the project directory, run:

```sh
cargo build --release
```
* Or, build using makefile:
```sh
make
```

* Cargo will **download and install** (Please make sure you have an internet connection) the required dependencies automatically

## Run

* In the project directory, run:

```sh
./target/release/csf363_interpreter 2> stderr_output
```
* Or, if built using Makefile:
```sh
./final_bin 2> stderr_output
```
* Or, run with makefile (redirect `stderr` as necessary):
```sh
make run 2> stderr_output
```

* The program will launch in the terminal

## Features

### Errors
* Prints detailed error messages for Parsing and Semantic Errors, and prints the final AST built from the input for successful commands.

### Game
* A clean interface for `stdout`, with expected output on `stderr`

### Help

* A `HELP.` command is provided for details of the Game and Interpreter.