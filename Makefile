all:
	@cargo build --release
	@cp ./target/release/csf363_interpreter ./final_bin

run:
	./final_bin

clean:
	@cargo clean
	@rm ./final_bin ./stderr_output

deps_install:
	@curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
	@rustup default stable
	@rustup update