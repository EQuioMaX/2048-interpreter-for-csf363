use super::error;
use super::game;
use super::parse::*;
use rustyline::{error::ReadlineError, Editor};
use termion::color;

pub fn init() {
    let mut rl = Editor::<()>::new();
    if rl.load_history("history.log").is_err() {
        println!("No previous history.");
    }

    let mut board = game::Board::new();
    board.init();
    board.print();
    loop {
        let readline = rl.readline("\n[2048]>> ");
        match readline {
            Ok(line) => {
                rl.add_history_entry(line.as_str());
                let _res = parse_expr(&line, Rule::inp);
                match _res {
                    Ok(v) => {
                        println!(
                            "{}Parsing Successful.\nFinal AST:{} {}{:?}{}",
                            color::Fg(color::LightGreen),
                            color::Fg(color::Reset),
                            color::Fg(color::LightWhite),
                            v,
                            color::Fg(color::Reset)
                        );
                        board.eval_ast(v);
                    }
                    Err(e) => {
                        error::error_handler(e);
                    }
                }
            }
            Err(ReadlineError::Interrupted) => {
                println!("SIGINT! Exiting...");
                break;
            }
            Err(ReadlineError::Eof) => {
                println!("EOF! Exiting...");
                break;
            }
            Err(err) => {
                println!("Error: {:?}", err);
                break;
            }
        }
    }
    rl.save_history("history.log").unwrap();
}
