#[macro_use]
extern crate pest_derive;
extern crate termion;
use termion::{color, style};

mod error;
mod game;
mod parse;
mod shell;
fn main() {
    shell::init();
}

fn print_help() {
    println!(
        "{}{}[2048 Interpreter v0.1.0 for CSF363]{}{}",
        color::Fg(color::Cyan),
        style::Bold,
        style::Reset,
        color::Fg(color::Reset)
    );
    println!("A simple interpreter for a variant of the 2048 game written in Rust.\n");
    println!(
        "{}Syntax: All uppercase, a period \".\" for EOI{}",
        color::Fg(color::LightGreen),
        color::Fg(color::Reset)
    );
    println!(
        "\n{}Commands:{}",
        color::Fg(color::LightYellow),
        color::Fg(color::Reset)
    );
    println!(
        "\t{}1. Moves:{} ADD, SUBTRACT, MULTIPLY, DIVIDE",
        color::Fg(color::LightYellow),
        color::Fg(color::Reset)
    );
    println!("\tExecutes a move on the game board.");
    println!(
        "\t\t{}Arguments:{} A Direction (UP, DOWN, LEFT, RIGHT)",
        color::Fg(color::LightYellow),
        color::Fg(color::Reset)
    );
    println!(
        "\t\t{}Example:{} \"ADD LEFT.\"",
        color::Fg(color::LightYellow),
        color::Fg(color::Reset)
    );
    println!(
        "\n\t{}2. Assignment:{} ASSIGN {}<value>{} TO {}<location>{}.",
        color::Fg(color::LightYellow),
        color::Fg(color::Reset),
        color::Fg(color::LightMagenta),
        color::Fg(color::Reset),
        color::Fg(color::LightMagenta),
        color::Fg(color::Reset)
    );
    println!("\tAssigns a value to location on the game board.");
    println!(
        "\t\t{}Arguments:{} A value (integer), and a location on the game board (1,1 to 4,4).",
        color::Fg(color::LightYellow),
        color::Fg(color::Reset)
    );
    println!(
        "\t\t{}Example:{} \"ASSIGN 7 TO 2,3.\"",
        color::Fg(color::LightYellow),
        color::Fg(color::Reset)
    );
    println!(
        "\n\t{}3. Naming:{} VAR {}<varname>{} IS {}<location>{}.",
        color::Fg(color::LightYellow),
        color::Fg(color::Reset),
        color::Fg(color::LightMagenta),
        color::Fg(color::Reset),
        color::Fg(color::LightMagenta),
        color::Fg(color::Reset)
    );
    println!("\tNames the tile on the given location on the game board.");
    println!(
        "\t\t{}Arguments:{} A variable name (identifier) and a location on the game board (1,1 to 4,4).",
        color::Fg(color::LightYellow),
        color::Fg(color::Reset));
    println!(
        "\t\t{}Example:{} \"VAR test IS 3,1.\"",
        color::Fg(color::LightYellow),
        color::Fg(color::Reset)
    );
    println!(
        "\n\t{}4. Query:{} VALUE IN {}<location>{}.",
        color::Fg(color::LightYellow),
        color::Fg(color::Reset),
        color::Fg(color::LightMagenta),
        color::Fg(color::Reset)
    );
    println!("\tQueries the value of the tile on the given location on the game board. Can be used with {}<Assignment>{} to assign values on the board.",
    color::Fg(color::LightYellow),
    color::Fg(color::Reset));
    println!(
        "\t\t{}Arguments:{} A location on the game board (1,1 to 4,4)",
        color::Fg(color::LightYellow),
        color::Fg(color::Reset)
    );
    println!(
        "\t\t{}Example:{} \"VALUE IN 3,1.\"",
        color::Fg(color::LightYellow),
        color::Fg(color::Reset)
    );
}
