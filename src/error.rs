use super::parse::Rule;
use pest::error::*;
use termion::{color, style};

pub fn error_handler(e: Error<Rule>) {
    eprintln!("-1");
    println!(
        "{}{}Parsing Error: {}{}{}{}{}",
        color::Fg(color::LightRed),
        style::Bold,
        style::Reset,
        color::Fg(color::Reset),
        color::Fg(color::LightWhite),
        e,
        color::Fg(color::Reset),
    );
    let ev = e.variant;
    match ev {
        ErrorVariant::ParsingError {
            positives,
            negatives,
        } => {
            for i in positives {
                rule_error(i);
            }
            for _i in negatives {}
        }
        ErrorVariant::CustomError { message } => {
            println!("{}", message);
        }
    }
}

pub fn rule_error(e: Rule) {
    match e {
        Rule::inp => {
            println!("{}Syntax Error, please check if your command is spelled correctly, in UPPERCASE, and ends with a period \".\".{}", color::Fg(color::LightYellow),
            color::Fg(color::Reset));
        }
        Rule::expr => {
            println!(
                "{}Invalid argument or expression. Write \"HELP.\" for Command List.{}",
                color::Fg(color::LightYellow),
                color::Fg(color::Reset)
            );
        }
        Rule::mov => {
            println!(
                "{}Move Error.{}",
                color::Fg(color::LightYellow),
                color::Fg(color::Reset)
            );
        }
        Rule::name => {
            println!(
                "{}Naming Error.{}",
                color::Fg(color::LightYellow),
                color::Fg(color::Reset)
            );
        }
        Rule::assignment => {
            println!(
                "{}Assignment Error.{}",
                color::Fg(color::LightYellow),
                color::Fg(color::Reset)
            );
        }
        Rule::query => {
            println!(
                "{}Invalid Query.{}",
                color::Fg(color::LightYellow),
                color::Fg(color::Reset)
            );
        }
        Rule::loc => {
            println!("{}Invalid Tile Location. Please make sure the location is formatted as \"loc1,loc2\".{}", color::Fg(color::LightYellow),
            color::Fg(color::Reset));
        }
        Rule::dim => {
            println!(
                "{}Invalid Location Bounds. Please make sure the coordinates are > 0 and <= 4.{}",
                color::Fg(color::LightYellow),
                color::Fg(color::Reset)
            );
        }
        Rule::identifier => {
            println!("{}Invalid Identifier. Please make sure the identifier is alphanumeric and starts with a letter or an underscore (_).{}", color::Fg(color::LightYellow),
            color::Fg(color::Reset));
        }
        Rule::dir => {
            println!("{}Invalid Direction. Please make sure the direction is written in UPPERCASE and one of (UP, DOWN, LEFT, RIGHT).{}", color::Fg(color::LightYellow),
            color::Fg(color::Reset));
        }
        Rule::EOI => {
            println!("{}Unexpected Input after command. Please make sure your input contains only one command.{}", color::Fg(color::LightYellow),
            color::Fg(color::Reset));
        }
        Rule::num => {
            println!("{}Invalid Number. Make sure the argument is a positive integer.{}", color::Fg(color::LightYellow),
            color::Fg(color::Reset));
        }
        _ => {
            rule_error(Rule::expr);
        }
    }
}
