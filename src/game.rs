use super::parse::*;
use super::parse::{Dir, Op};
use rand::prelude::*;
use std::collections::HashMap;

use termion::{color, style};

#[derive(PartialEq, Clone, Debug, Copy)]
pub struct Tile {
    pub x: i32,
    pub y: i32,
    pub value: i32,
    pub exists: bool,
    pub merged_from: Option<((usize, usize), (usize, usize))>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Board {
    arr: [[Tile; 4]; 4],
    labels: HashMap<String, Tile>,
}

impl Board {
    pub fn new() -> Self {
        Board {
            arr: [[Tile {
                x: -1,
                y: -1,
                value: -1,
                exists: false,
                merged_from: None,
            }; 4]; 4],
            labels: HashMap::new(),
        }
    }
    pub fn init(&mut self) {
        let mut rng = thread_rng();
        let pos1 = rng.gen_range(0..3);
        let pos2 = rng.gen_range(0..3);
        self.arr[pos1][pos2].value = 2;
        self.arr[pos1][pos2].exists = true;
        for i in 0..=3 {
            for j in 0..=3 {
                self.arr[i][j].x = i as i32;
                self.arr[i][j].y = j as i32;
            }
        }
    }

    fn insert_rand_tile(&mut self) {
        println!("Inserting Random Tile...");
        let mut rng = thread_rng();
        let mut pos1 = rng.gen_range(0..3);
        let mut pos2 = rng.gen_range(0..3);
        while self.arr[pos1][pos2].exists {
            pos1 = rng.gen_range(0..3);
            pos2 = rng.gen_range(0..3);
        }
        self.arr[pos1][pos2].exists = true;
        self.arr[pos1][pos2].value = 2;
    }

    pub fn print(&self) {
        for i in 0..=3 {
            println!("{}-----------------------------{}", color::Fg(color::LightCyan), color::Fg(color::Reset));
            print!("{}|{}", color::Fg(color::LightCyan), color::Fg(color::Reset));
            for j in 0..=3 {
                if self.arr[i][j].exists {
                    print!("{0} {1:<4} {2}|{3}", color::Fg(color::LightYellow), self.arr[i][j].value, color::Fg(color::LightCyan), color::Fg(color::Reset));
                    if i == 3 && j == 3 {
                        eprint!("{}", self.arr[i][j].value);
                    } else {
                        eprint!("{}\x20", self.arr[i][j].value);
                    }
                } else {
                    print!(" {0:<4} {1}|{2}", " ", color::Fg(color::LightCyan), color::Fg(color::Reset));
                    if i == 3 && j == 3 {
                        eprint!("{}", 0);
                    } else {
                        eprint!("{}\x20", 0);
                    }
                }
            }
            println!();
        }
        println!("{}-----------------------------{}", color::Fg(color::LightCyan), color::Fg(color::Reset));
        for i in 0..=3 {
            for j in 0..=3 {
                let mut v = Vec::new();
                for s in self.labels.iter() {
                    let (l, m) = s;
                    if m.x == i as i32 && m.y == j as i32 {
                        v.push(l.clone());
                    }
                }
                if !v.is_empty() {
                    print!("{}{},{}:{} ", color::Fg(color::LightMagenta),i + 1, j + 1, color::Fg(color::Reset));
                    eprint!("\x20{},{}", i + 1, j + 1);
                }
                let mut counter = 0;
                let lim = v.len();
                for s in v {
                    if counter + 1 == lim {
                        eprint!("{}", s);
                        println!("{}{}{}", color::Fg(color::LightBlue), s, color::Fg(color::Reset));
                    } else {
                        print!("{}{}{}, ", color::Fg(color::LightBlue), s, color::Fg(color::Reset));
                        eprint!("{},", s);
                    }
                    counter += 1;
                }
            }
        }
        println!();
        eprintln!();
    }

    fn mov(&mut self, op: Op, dir: Dir) {
        let mut b = [[0; 4]; 4];
        let mut stale = false;
        for i in 0..4 {
            for j in 0..4 {
                if self.arr[i][j].exists {
                    b[i][j] = self.arr[i][j].value;
                } else {
                    b[i][j] = -1;
                }
            }
        }
        match op {
            Op::Add => self.add(dir),
            Op::Subtract => self.subtract(dir),
            Op::Multiply => self.multiply(dir),
            Op::Divide => self.divide(dir),
        }
        for i in 0..4 {
            for j in 0..4 {
                if self.arr[i][j].exists {
                    if b[i][j] != self.arr[i][j].value {
                        stale = true;
                        break;
                    }
                } else if b[i][j] != -1 {
                    stale = true;
                    break;
                }
            }
        }
        if stale {
            self.insert_rand_tile();
        }
        self.print();
    }

    fn add(&mut self, dir: Dir) {
        match dir {
            Dir::Up => {
                for j in 0..=3 {
                    for i in 0..=2 {
                        let mut next_row = i + 1;
                        while next_row < 4 && !self.arr[next_row][j].exists {
                            next_row += 1;
                        }
                        if next_row == 4 {
                            break;
                        }

                        let mut test = next_row;

                        if i == 0 && !self.arr[i][j].exists {
                            next_row = i;
                            for k in next_row..4 {
                                while test < 4 && !self.arr[test][j].exists {
                                    test += 1;
                                }
                                if test == 4 {
                                    break;
                                }
                                self.arr[k][j].value = self.arr[test][j].value;
                                self.arr[k][j].exists = true;
                                self.arr[test][j].exists = false;
                                let mut v2 = Vec::new();
                                for s in self.labels.iter() {
                                    let (l, m) = s;
                                    if m.x == test as i32 && m.y == j as i32 {
                                        v2.push(l.clone());
                                    }
                                }
                                for s in v2 {
                                    self.labels.remove(&s);
                                    self.labels.insert(s, self.arr[k][j]);
                                }
                            }
                            next_row = 1;
                            while next_row < 4 && !self.arr[next_row][j].exists {
                                next_row += 1;
                            }
                            if next_row == 4 {
                                break;
                            }
                        }

                        //Add the next tile
                        if self.arr[i][j].exists
                            && self.arr[i][j].value == self.arr[next_row][j].value
                        {
                            self.arr[i][j].value += self.arr[next_row][j].value;
                            self.arr[next_row][j].exists = false;

                            let mut v = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if m.x == next_row as i32 && m.y == j as i32 {
                                    v.push(l.clone());
                                }
                            }

                            for s in v {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[i][j]);
                            }
                        } else if self.arr[i][j].exists {
                            continue;
                        } else {
                            next_row = i;
                        }
                        //Compress
                        for k in next_row..4 {
                            while test < 4 && !self.arr[test][j].exists {
                                test += 1;
                            }
                            if test == 4 {
                                break;
                            }
                            self.arr[k][j].value = self.arr[test][j].value;
                            self.arr[k][j].exists = true;
                            self.arr[test][j].exists = false;
                            let mut v2 = Vec::new();
                            for s in self.labels.iter() {
                                let (l, m) = s;
                                if m.x == test as i32 && m.y == j as i32 {
                                    v2.push(l.clone());
                                }
                            }
                            for s in v2 {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[k][j]);
                            }
                        }
                    }
                }
            }
            Dir::Down => {
                for j in 0..=3 {
                    for i in (1..=3).rev() {
                        let mut next_row: usize = i - 1;
                        let mut of = false;
                        while !self.arr[next_row][j].exists {
                            if next_row != 0 {
                                next_row -= 1;
                            } else {
                                of = true;
                                break;
                            }
                        }
                        if of {
                            break;
                        }

                        let mut test = next_row;

                        if i == 3 && !self.arr[i][j].exists {
                            next_row = i;
                            for k in (0..=next_row).rev() {
                                while !self.arr[test][j].exists {
                                    if test != 0 {
                                        test -= 1;
                                    } else {
                                        of = true;
                                        break;
                                    }
                                }
                                if of {
                                    break;
                                }
                                self.arr[k][j].value = self.arr[test][j].value;
                                self.arr[k][j].exists = true;
                                self.arr[test][j].exists = false;

                                let mut v2 = Vec::new();
                                for k in self.labels.iter() {
                                    let (l, m) = k;
                                    if m.x == test as i32 && m.y == j as i32 {
                                        v2.push(l.clone());
                                    }
                                }
                                for s in v2 {
                                    self.labels.remove(&s);
                                    self.labels.insert(s, self.arr[k][j]);
                                }
                            }
                            next_row = 2;
                            of = false;
                            while !self.arr[next_row][j].exists {
                                if next_row != 0 {
                                    next_row -= 1;
                                } else {
                                    of = true;
                                    break;
                                }
                            }
                            if of {
                                break;
                            }
                        }
                        //Add the next tile
                        if self.arr[i][j].exists
                            && self.arr[i][j].value == self.arr[next_row][j].value
                        {
                            self.arr[i][j].value += self.arr[next_row][j].value;
                            self.arr[next_row][j].exists = false;

                            let mut v = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if m.x == next_row as i32 && m.y == j as i32 {
                                    v.push(l.clone());
                                }
                            }

                            for s in v {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[i][j]);
                            }
                        } else if self.arr[i][j].exists {
                            continue;
                        } else {
                            next_row = i;
                        }
                        of = false;
                        //Compress
                        for k in (0..=next_row).rev() {
                            while !self.arr[test][j].exists {
                                if test != 0 {
                                    test -= 1;
                                } else {
                                    of = true;
                                    break;
                                }
                            }
                            if of {
                                break;
                            }
                            self.arr[k][j].value = self.arr[test][j].value;
                            self.arr[k][j].exists = true;
                            self.arr[test][j].exists = false;

                            let mut v2 = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if m.x == test as i32 && m.y == j as i32 {
                                    v2.push(l.clone());
                                }
                            }
                            for s in v2 {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[k][j]);
                            }
                        }
                    }
                }
            }
            Dir::Left => {
                for i in 0..=3 {
                    for j in 0..=2 {
                        let mut next_col = j + 1;
                        while next_col < 4 && !self.arr[i][next_col].exists {
                            next_col += 1;
                        }
                        if next_col == 4 {
                            break;
                        }

                        let mut test = next_col;

                        if j == 0 && !self.arr[i][j].exists {
                            next_col = j;
                            for k in next_col..4 {
                                while test < 4 && !self.arr[i][test].exists {
                                    test += 1;
                                }
                                if test == 4 {
                                    break;
                                }
                                self.arr[i][k].value = self.arr[i][test].value;
                                self.arr[i][k].exists = true;
                                self.arr[i][test].exists = false;
                                let mut v2 = Vec::new();
                                for t in self.labels.iter() {
                                    let (l, m) = t;
                                    if m.x == i as i32 && m.y == test as i32 {
                                        v2.push(l.clone());
                                    }
                                }
                                for s in v2 {
                                    self.labels.remove(&s);
                                    self.labels.insert(s, self.arr[i][k]);
                                }
                            }
                            next_col = 1;
                            while next_col < 4 && !self.arr[i][next_col].exists {
                                next_col += 1;
                            }
                            if next_col == 4 {
                                break;
                            }
                        }
                        //Add the next tile
                        if self.arr[i][j].exists
                            && self.arr[i][j].value == self.arr[i][next_col].value
                        {
                            self.arr[i][j].value += self.arr[i][next_col].value;
                            self.arr[i][next_col].exists = false;

                            let mut v = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if m.x == i as i32 && m.y == next_col as i32 {
                                    v.push(l.clone());
                                }
                            }

                            for s in v {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[i][j]);
                            }
                        } else if self.arr[i][j].exists {
                            continue;
                        } else {
                            next_col = j;
                        }
                        //Compress
                        for k in next_col..4 {
                            while test < 4 && !self.arr[i][test].exists {
                                test += 1;
                            }
                            if test == 4 {
                                break;
                            }
                            self.arr[i][k].value = self.arr[i][test].value;
                            self.arr[i][k].exists = true;
                            self.arr[i][test].exists = false;
                            let mut v2 = Vec::new();
                            for t in self.labels.iter() {
                                let (l, m) = t;
                                if m.x == i as i32 && m.y == test as i32 {
                                    v2.push(l.clone());
                                }
                            }
                            for s in v2 {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[i][k]);
                            }
                        }
                    }
                }
            }
            Dir::Right => {
                for i in 0..=3 {
                    for j in (1..=3).rev() {
                        let mut next_col: usize = j - 1;
                        let mut of = false;
                        while !self.arr[i][next_col].exists {
                            if next_col != 0 {
                                next_col -= 1;
                            } else {
                                of = true;
                                break;
                            }
                        }
                        if of {
                            break;
                        }
                        let mut test = next_col;

                        if j == 3 && !self.arr[i][j].exists {
                            next_col = j;
                            for k in (0..=next_col).rev() {
                                while !self.arr[i][test].exists {
                                    if test != 0 {
                                        test -= 1;
                                    } else {
                                        of = true;
                                        break;
                                    }
                                }
                                if of {
                                    break;
                                }
                                //println!("Next element: {},{}; k = {};", i , test, k);
                                self.arr[i][k].value = self.arr[i][test].value;
                                self.arr[i][k].exists = true;
                                self.arr[i][test].exists = false;

                                let mut v2 = Vec::new();
                                for k in self.labels.iter() {
                                    let (l, m) = k;
                                    if m.x == i as i32 && m.y == test as i32 {
                                        v2.push(l.clone());
                                    }
                                }
                                for s in v2 {
                                    self.labels.remove(&s);
                                    self.labels.insert(s, self.arr[i][k]);
                                }
                            }
                            next_col = 2;
                            of = false;
                            while !self.arr[i][next_col].exists {
                                if next_col != 0 {
                                    next_col -= 1;
                                } else {
                                    of = true;
                                    break;
                                }
                            }
                            if of {
                                break;
                            }
                        }

                        //Add the next tile
                        if self.arr[i][j].exists
                            && self.arr[i][j].value == self.arr[i][next_col].value
                        {
                            self.arr[i][j].value += self.arr[i][next_col].value;
                            self.arr[i][next_col].exists = false;

                            let mut v = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if m.x == i as i32 && m.y == next_col as i32 {
                                    v.push(l.clone());
                                }
                            }

                            for s in v {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[i][j]);
                            }
                        } else if self.arr[i][j].exists {
                            continue;
                        } else {
                            next_col = j;
                        }
                        of = false;
                        //Compress
                        for k in (0..=next_col).rev() {
                            while !self.arr[i][test].exists {
                                if test != 0 {
                                    test -= 1;
                                } else {
                                    of = true;
                                    break;
                                }
                            }
                            if of {
                                break;
                            }
                            self.arr[i][k].value = self.arr[i][test].value;
                            self.arr[i][k].exists = true;
                            self.arr[i][test].exists = false;

                            let mut v2 = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if m.x == i as i32 && m.y == test as i32 {
                                    v2.push(l.clone());
                                }
                            }
                            for s in v2 {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[i][k]);
                            }
                        }
                    }
                }
            }
        }
    }

    fn subtract(&mut self, dir: Dir) {
        match dir {
            Dir::Up => {
                for j in 0..=3 {
                    for i in 0..=2 {
                        let mut next_row = i + 1;
                        while next_row < 4 && !self.arr[next_row][j].exists {
                            next_row += 1;
                        }
                        if next_row == 4 {
                            break;
                        }

                        let mut test = next_row;

                        if i == 0 && !self.arr[i][j].exists {
                            next_row = i;
                            for k in next_row..4 {
                                while test < 4 && !self.arr[test][j].exists {
                                    test += 1;
                                }
                                if test == 4 {
                                    break;
                                }
                                self.arr[k][j].value = self.arr[test][j].value;
                                self.arr[k][j].exists = true;
                                self.arr[test][j].exists = false;
                                let mut v2 = Vec::new();
                                for s in self.labels.iter() {
                                    let (l, m) = s;
                                    if m.x == test as i32 && m.y == j as i32 {
                                        v2.push(l.clone());
                                    }
                                }
                                for s in v2 {
                                    self.labels.remove(&s);
                                    self.labels.insert(s, self.arr[k][j]);
                                }
                            }
                            next_row = 1;
                            while next_row < 4 && !self.arr[next_row][j].exists {
                                next_row += 1;
                            }
                            if next_row == 4 {
                                break;
                            }
                        }

                        //Add the next tile
                        if self.arr[i][j].exists
                            && self.arr[i][j].value == self.arr[next_row][j].value
                        {
                            self.arr[i][j].value = 0;
                            self.arr[i][j].exists = false;
                            self.arr[next_row][j].exists = false;

                            let mut v = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if (m.x == next_row as i32 || m.x == i as i32) && m.y == j as i32 {
                                    v.push(l.clone());
                                }
                            }

                            for s in v {
                                self.labels.remove(&s);
                            }
                        } else if self.arr[i][j].exists {
                            continue;
                        } else {
                            next_row = i;
                        }
                        //Compress
                        for k in next_row..4 {
                            while test < 4 && !self.arr[test][j].exists {
                                test += 1;
                            }
                            if test == 4 {
                                break;
                            }
                            self.arr[k][j].value = self.arr[test][j].value;
                            self.arr[k][j].exists = true;
                            self.arr[test][j].exists = false;
                            let mut v2 = Vec::new();
                            for s in self.labels.iter() {
                                let (l, m) = s;
                                if m.x == test as i32 && m.y == j as i32 {
                                    v2.push(l.clone());
                                }
                            }
                            for s in v2 {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[k][j]);
                            }
                        }
                    }
                }
            }
            Dir::Down => {
                for j in 0..=3 {
                    for i in (1..=3).rev() {
                        let mut next_row: usize = i - 1;
                        let mut of = false;
                        while !self.arr[next_row][j].exists {
                            if next_row != 0 {
                                next_row -= 1;
                            } else {
                                of = true;
                                break;
                            }
                        }
                        if of {
                            break;
                        }

                        let mut test = next_row;

                        if i == 3 && !self.arr[i][j].exists {
                            next_row = i;
                            for k in (0..=next_row).rev() {
                                while !self.arr[test][j].exists {
                                    if test != 0 {
                                        test -= 1;
                                    } else {
                                        of = true;
                                        break;
                                    }
                                }
                                if of {
                                    break;
                                }
                                self.arr[k][j].value = self.arr[test][j].value;
                                self.arr[k][j].exists = true;
                                self.arr[test][j].exists = false;

                                let mut v2 = Vec::new();
                                for k in self.labels.iter() {
                                    let (l, m) = k;
                                    if m.x == test as i32 && m.y == j as i32 {
                                        v2.push(l.clone());
                                    }
                                }
                                for s in v2 {
                                    self.labels.remove(&s);
                                    self.labels.insert(s, self.arr[k][j]);
                                }
                            }
                            next_row = 2;
                            of = false;
                            while !self.arr[next_row][j].exists {
                                if next_row != 0 {
                                    next_row -= 1;
                                } else {
                                    of = true;
                                    break;
                                }
                            }
                            if of {
                                break;
                            }
                        }

                        //Add the next tile
                        if self.arr[i][j].exists
                            && self.arr[i][j].value == self.arr[next_row][j].value
                        {
                            self.arr[i][j].value = 0;
                            self.arr[i][j].exists = false;
                            self.arr[next_row][j].exists = false;

                            let mut v = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if (m.x == next_row as i32 || m.x == i as i32) && m.y == j as i32 {
                                    v.push(l.clone());
                                }
                            }
                            for s in v {
                                self.labels.remove(&s);
                            }
                        } else if self.arr[i][j].exists {
                            continue;
                        } else {
                            next_row = i;
                        }
                        of = false;
                        //Compress
                        for k in (0..=next_row).rev() {
                            while !self.arr[test][j].exists {
                                if test != 0 {
                                    test -= 1;
                                } else {
                                    of = true;
                                    break;
                                }
                            }
                            if of {
                                break;
                            }
                            self.arr[k][j].value = self.arr[test][j].value;
                            self.arr[k][j].exists = true;
                            self.arr[test][j].exists = false;

                            let mut v2 = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if m.x == test as i32 && m.y == j as i32 {
                                    v2.push(l.clone());
                                }
                            }
                            for s in v2 {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[k][j]);
                            }
                        }
                    }
                }
            }
            Dir::Left => {
                for i in 0..=3 {
                    for j in 0..=2 {
                        let mut next_col = j + 1;
                        while next_col < 4 && !self.arr[i][next_col].exists {
                            next_col += 1;
                        }
                        if next_col == 4 {
                            break;
                        }

                        let mut test = next_col;

                        if j == 0 && !self.arr[i][j].exists {
                            next_col = j;
                            for k in next_col..4 {
                                while test < 4 && !self.arr[i][test].exists {
                                    test += 1;
                                }
                                if test == 4 {
                                    break;
                                }
                                self.arr[i][k].value = self.arr[i][test].value;
                                self.arr[i][k].exists = true;
                                self.arr[i][test].exists = false;
                                let mut v2 = Vec::new();
                                for t in self.labels.iter() {
                                    let (l, m) = t;
                                    if m.x == i as i32 && m.y == test as i32 {
                                        v2.push(l.clone());
                                    }
                                }
                                for s in v2 {
                                    self.labels.remove(&s);
                                    self.labels.insert(s, self.arr[i][k]);
                                }
                            }
                            next_col = 1;
                            while next_col < 4 && !self.arr[i][next_col].exists {
                                next_col += 1;
                            }
                            if next_col == 4 {
                                break;
                            }
                        }
                        //Add the next tile
                        if self.arr[i][j].exists
                            && self.arr[i][j].value == self.arr[i][next_col].value
                        {
                            self.arr[i][j].value = 0;
                            self.arr[i][j].exists = false;
                            self.arr[i][next_col].exists = false;

                            let mut v = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if m.x == i as i32 && (m.y == next_col as i32 || m.y == j as i32) {
                                    v.push(l.clone());
                                }
                            }
                            for s in v {
                                self.labels.remove(&s);
                            }
                        } else if self.arr[i][j].exists {
                            continue;
                        } else {
                            next_col = j;
                        }
                        //Compress
                        for k in next_col..4 {
                            while test < 4 && !self.arr[i][test].exists {
                                test += 1;
                            }
                            if test == 4 {
                                break;
                            }
                            self.arr[i][k].value = self.arr[i][test].value;
                            self.arr[i][k].exists = true;
                            self.arr[i][test].exists = false;
                            let mut v2 = Vec::new();
                            for t in self.labels.iter() {
                                let (l, m) = t;
                                if m.x == i as i32 && m.y == test as i32 {
                                    v2.push(l.clone());
                                }
                            }
                            for s in v2 {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[i][k]);
                            }
                        }
                    }
                }
            }
            Dir::Right => {
                for i in 0..=3 {
                    for j in (1..=3).rev() {
                        let mut next_col: usize = j - 1;
                        let mut of = false;
                        while !self.arr[i][next_col].exists {
                            if next_col != 0 {
                                next_col -= 1;
                            } else {
                                of = true;
                                break;
                            }
                        }
                        if of {
                            break;
                        }

                        let mut test = next_col;

                        if j == 3 && !self.arr[i][j].exists {
                            next_col = j;
                            for k in (0..=next_col).rev() {
                                while !self.arr[i][test].exists {
                                    if test != 0 {
                                        test -= 1;
                                    } else {
                                        of = true;
                                        break;
                                    }
                                }
                                if of {
                                    break;
                                }
                                self.arr[i][k].value = self.arr[i][test].value;
                                self.arr[i][k].exists = true;
                                self.arr[i][test].exists = false;

                                let mut v2 = Vec::new();
                                for k in self.labels.iter() {
                                    let (l, m) = k;
                                    if m.x == i as i32 && m.y == test as i32 {
                                        v2.push(l.clone());
                                    }
                                }
                                for s in v2 {
                                    self.labels.remove(&s);
                                    self.labels.insert(s, self.arr[i][k]);
                                }
                            }
                            next_col = 2;
                            of = false;
                            while !self.arr[i][next_col].exists {
                                if next_col != 0 {
                                    next_col -= 1;
                                } else {
                                    of = true;
                                    break;
                                }
                            }
                            if of {
                                break;
                            }
                        }

                        //Add the next tile
                        if self.arr[i][j].exists
                            && self.arr[i][j].value == self.arr[i][next_col].value
                        {
                            self.arr[i][j].value = 0;
                            self.arr[i][j].exists = false;
                            self.arr[i][next_col].exists = false;

                            let mut v = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if m.x == i as i32 && (m.y == next_col as i32 || m.y == j as i32) {
                                    v.push(l.clone());
                                }
                            }

                            for s in v {
                                self.labels.remove(&s);
                            }
                        } else if self.arr[i][j].exists {
                            continue;
                        } else {
                            next_col = j;
                        }
                        of = false;
                        //Compress
                        for k in (0..=next_col).rev() {
                            while !self.arr[i][test].exists {
                                if test != 0 {
                                    test -= 1;
                                } else {
                                    of = true;
                                    break;
                                }
                            }
                            if of {
                                break;
                            }
                            self.arr[i][k].value = self.arr[i][test].value;
                            self.arr[i][k].exists = true;
                            self.arr[i][test].exists = false;

                            let mut v2 = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if m.x == i as i32 && m.y == test as i32 {
                                    v2.push(l.clone());
                                }
                            }
                            for s in v2 {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[i][k]);
                            }
                        }
                    }
                }
            }
        }
    }

    fn multiply(&mut self, dir: Dir) {
        match dir {
            Dir::Up => {
                for j in 0..=3 {
                    for i in 0..=2 {
                        let mut next_row = i + 1;
                        while next_row < 4 && !self.arr[next_row][j].exists {
                            next_row += 1;
                        }
                        if next_row == 4 {
                            break;
                        }

                        let mut test = next_row;

                        if i == 0 && !self.arr[i][j].exists {
                            next_row = i;
                            for k in next_row..4 {
                                while test < 4 && !self.arr[test][j].exists {
                                    test += 1;
                                }
                                if test == 4 {
                                    break;
                                }
                                self.arr[k][j].value = self.arr[test][j].value;
                                self.arr[k][j].exists = true;
                                self.arr[test][j].exists = false;
                                let mut v2 = Vec::new();
                                for s in self.labels.iter() {
                                    let (l, m) = s;
                                    if m.x == test as i32 && m.y == j as i32 {
                                        v2.push(l.clone());
                                    }
                                }
                                for s in v2 {
                                    self.labels.remove(&s);
                                    self.labels.insert(s, self.arr[k][j]);
                                }
                            }
                            next_row = 1;
                            while next_row < 4 && !self.arr[next_row][j].exists {
                                next_row += 1;
                            }
                            if next_row == 4 {
                                break;
                            }
                        }

                        //Add the next tile
                        if self.arr[i][j].exists
                            && self.arr[i][j].value == self.arr[next_row][j].value
                        {
                            self.arr[i][j].value *= self.arr[next_row][j].value;
                            self.arr[next_row][j].exists = false;

                            let mut v = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if m.x == next_row as i32 && m.y == j as i32 {
                                    v.push(l.clone());
                                }
                            }

                            for s in v {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[i][j]);
                            }
                        } else if self.arr[i][j].exists {
                            continue;
                        } else {
                            next_row = i;
                        }
                        //Compress
                        for k in next_row..4 {
                            while test < 4 && !self.arr[test][j].exists {
                                test += 1;
                            }
                            if test == 4 {
                                break;
                            }
                            self.arr[k][j].value = self.arr[test][j].value;
                            self.arr[k][j].exists = true;
                            self.arr[test][j].exists = false;
                            let mut v2 = Vec::new();
                            for s in self.labels.iter() {
                                let (l, m) = s;
                                if m.x == test as i32 && m.y == j as i32 {
                                    v2.push(l.clone());
                                }
                            }
                            for s in v2 {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[k][j]);
                            }
                        }
                    }
                }
            }
            Dir::Down => {
                for j in 0..=3 {
                    for i in (1..=3).rev() {
                        let mut next_row: usize = i - 1;
                        let mut of = false;
                        while !self.arr[next_row][j].exists {
                            if next_row != 0 {
                                next_row -= 1;
                            } else {
                                of = true;
                                break;
                            }
                        }
                        if of {
                            break;
                        }

                        let mut test = next_row;

                        if i == 3 && !self.arr[i][j].exists {
                            next_row = i;
                            for k in (0..=next_row).rev() {
                                while !self.arr[test][j].exists {
                                    if test != 0 {
                                        test -= 1;
                                    } else {
                                        of = true;
                                        break;
                                    }
                                }
                                if of {
                                    break;
                                }
                                self.arr[k][j].value = self.arr[test][j].value;
                                self.arr[k][j].exists = true;
                                self.arr[test][j].exists = false;

                                let mut v2 = Vec::new();
                                for k in self.labels.iter() {
                                    let (l, m) = k;
                                    if m.x == test as i32 && m.y == j as i32 {
                                        v2.push(l.clone());
                                    }
                                }
                                for s in v2 {
                                    self.labels.remove(&s);
                                    self.labels.insert(s, self.arr[k][j]);
                                }
                            }
                            next_row = 2;
                            of = false;
                            while !self.arr[next_row][j].exists {
                                if next_row != 0 {
                                    next_row -= 1;
                                } else {
                                    of = true;
                                    break;
                                }
                            }
                            if of {
                                break;
                            }
                        }
                        //Add the next tile
                        if self.arr[i][j].exists
                            && self.arr[i][j].value == self.arr[next_row][j].value
                        {
                            self.arr[i][j].value *= self.arr[next_row][j].value;
                            self.arr[next_row][j].exists = false;

                            let mut v = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if m.x == next_row as i32 && m.y == j as i32 {
                                    v.push(l.clone());
                                }
                            }

                            for s in v {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[i][j]);
                            }
                        } else if self.arr[i][j].exists {
                            continue;
                        } else {
                            next_row = i;
                        }
                        of = false;
                        //Compress
                        for k in (0..=next_row).rev() {
                            while !self.arr[test][j].exists {
                                if test != 0 {
                                    test -= 1;
                                } else {
                                    of = true;
                                    break;
                                }
                            }
                            if of {
                                break;
                            }
                            self.arr[k][j].value = self.arr[test][j].value;
                            self.arr[k][j].exists = true;
                            self.arr[test][j].exists = false;

                            let mut v2 = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if m.x == test as i32 && m.y == j as i32 {
                                    v2.push(l.clone());
                                }
                            }
                            for s in v2 {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[k][j]);
                            }
                        }
                    }
                }
            }
            Dir::Left => {
                for i in 0..=3 {
                    for j in 0..=2 {
                        let mut next_col = j + 1;
                        while next_col < 4 && !self.arr[i][next_col].exists {
                            next_col += 1;
                        }
                        if next_col == 4 {
                            break;
                        }

                        let mut test = next_col;

                        if j == 0 && !self.arr[i][j].exists {
                            next_col = j;
                            for k in next_col..4 {
                                while test < 4 && !self.arr[i][test].exists {
                                    test += 1;
                                }
                                if test == 4 {
                                    break;
                                }
                                self.arr[i][k].value = self.arr[i][test].value;
                                self.arr[i][k].exists = true;
                                self.arr[i][test].exists = false;
                                let mut v2 = Vec::new();
                                for t in self.labels.iter() {
                                    let (l, m) = t;
                                    if m.x == i as i32 && m.y == test as i32 {
                                        v2.push(l.clone());
                                    }
                                }
                                for s in v2 {
                                    self.labels.remove(&s);
                                    self.labels.insert(s, self.arr[i][k]);
                                }
                            }
                            next_col = 1;
                            while next_col < 4 && !self.arr[i][next_col].exists {
                                next_col += 1;
                            }
                            if next_col == 4 {
                                break;
                            }
                        }
                        //Add the next tile
                        if self.arr[i][j].exists
                            && self.arr[i][j].value == self.arr[i][next_col].value
                        {
                            self.arr[i][j].value *= self.arr[i][next_col].value;
                            self.arr[i][next_col].exists = false;

                            let mut v = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if m.x == i as i32 && m.y == next_col as i32 {
                                    v.push(l.clone());
                                }
                            }

                            for s in v {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[i][j]);
                            }
                        } else if self.arr[i][j].exists {
                            continue;
                        } else {
                            next_col = j;
                        }
                        //Compress
                        for k in next_col..4 {
                            while test < 4 && !self.arr[i][test].exists {
                                test += 1;
                            }
                            if test == 4 {
                                break;
                            }
                            self.arr[i][k].value = self.arr[i][test].value;
                            self.arr[i][k].exists = true;
                            self.arr[i][test].exists = false;
                            let mut v2 = Vec::new();
                            for t in self.labels.iter() {
                                let (l, m) = t;
                                if m.x == i as i32 && m.y == test as i32 {
                                    v2.push(l.clone());
                                }
                            }
                            for s in v2 {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[i][k]);
                            }
                        }
                    }
                }
            }
            Dir::Right => {
                for i in 0..=3 {
                    for j in (1..=3).rev() {
                        let mut next_col: usize = j - 1;
                        let mut of = false;
                        while !self.arr[i][next_col].exists {
                            if next_col != 0 {
                                next_col -= 1;
                            } else {
                                of = true;
                                break;
                            }
                        }
                        if of {
                            break;
                        }

                        let mut test = next_col;

                        if j == 3 && !self.arr[i][j].exists {
                            next_col = j;
                            for k in (0..=next_col).rev() {
                                while !self.arr[i][test].exists {
                                    if test != 0 {
                                        test -= 1;
                                    } else {
                                        of = true;
                                        break;
                                    }
                                }
                                if of {
                                    break;
                                }
                                self.arr[i][k].value = self.arr[i][test].value;
                                self.arr[i][k].exists = true;
                                self.arr[i][test].exists = false;

                                let mut v2 = Vec::new();
                                for k in self.labels.iter() {
                                    let (l, m) = k;
                                    if m.x == i as i32 && m.y == test as i32 {
                                        v2.push(l.clone());
                                    }
                                }
                                for s in v2 {
                                    self.labels.remove(&s);
                                    self.labels.insert(s, self.arr[i][k]);
                                }
                            }
                            next_col = 2;
                            of = false;
                            while !self.arr[i][next_col].exists {
                                if next_col != 0 {
                                    next_col -= 1;
                                } else {
                                    of = true;
                                    break;
                                }
                            }
                            if of {
                                break;
                            }
                        }

                        //Add the next tile
                        if self.arr[i][j].exists
                            && self.arr[i][j].value == self.arr[i][next_col].value
                        {
                            self.arr[i][j].value *= self.arr[i][next_col].value;
                            self.arr[i][next_col].exists = false;

                            let mut v = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if m.x == i as i32 && m.y == next_col as i32 {
                                    v.push(l.clone());
                                }
                            }

                            for s in v {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[i][j]);
                            }
                        } else if self.arr[i][j].exists {
                            continue;
                        } else {
                            next_col = j;
                        }
                        of = false;
                        //Compress
                        for k in (0..=next_col).rev() {
                            while !self.arr[i][test].exists {
                                if test != 0 {
                                    test -= 1;
                                } else {
                                    of = true;
                                    break;
                                }
                            }
                            if of {
                                break;
                            }
                            self.arr[i][k].value = self.arr[i][test].value;
                            self.arr[i][k].exists = true;
                            self.arr[i][test].exists = false;

                            let mut v2 = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if m.x == i as i32 && m.y == test as i32 {
                                    v2.push(l.clone());
                                }
                            }
                            for s in v2 {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[i][k]);
                            }
                        }
                    }
                }
            }
        }
    }

    fn divide(&mut self, dir: Dir) {
        match dir {
            Dir::Up => {
                for j in 0..=3 {
                    for i in 0..=2 {
                        let mut next_row = i + 1;
                        while next_row < 4 && !self.arr[next_row][j].exists {
                            next_row += 1;
                        }
                        if next_row == 4 {
                            break;
                        }

                        let mut test = next_row;

                        if i == 0 && !self.arr[i][j].exists {
                            next_row = i;
                            for k in next_row..4 {
                                while test < 4 && !self.arr[test][j].exists {
                                    test += 1;
                                }
                                if test == 4 {
                                    break;
                                }
                                self.arr[k][j].value = self.arr[test][j].value;
                                self.arr[k][j].exists = true;
                                self.arr[test][j].exists = false;
                                let mut v2 = Vec::new();
                                for s in self.labels.iter() {
                                    let (l, m) = s;
                                    if m.x == test as i32 && m.y == j as i32 {
                                        v2.push(l.clone());
                                    }
                                }
                                for s in v2 {
                                    self.labels.remove(&s);
                                    self.labels.insert(s, self.arr[k][j]);
                                }
                            }
                            next_row = 1;
                            while next_row < 4 && !self.arr[next_row][j].exists {
                                next_row += 1;
                            }
                            if next_row == 4 {
                                break;
                            }
                        }

                        //Add the next tile
                        if self.arr[i][j].exists
                            && self.arr[i][j].value == self.arr[next_row][j].value
                        {
                            self.arr[i][j].value /= self.arr[next_row][j].value;
                            self.arr[next_row][j].exists = false;

                            let mut v = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if m.x == next_row as i32 && m.y == j as i32 {
                                    v.push(l.clone());
                                }
                            }

                            for s in v {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[i][j]);
                            }
                        } else if self.arr[i][j].exists {
                            continue;
                        } else {
                            next_row = i;
                        }
                        //Compress
                        for k in next_row..4 {
                            while test < 4 && !self.arr[test][j].exists {
                                test += 1;
                            }
                            if test == 4 {
                                break;
                            }
                            self.arr[k][j].value = self.arr[test][j].value;
                            self.arr[k][j].exists = true;
                            self.arr[test][j].exists = false;
                            let mut v2 = Vec::new();
                            for s in self.labels.iter() {
                                let (l, m) = s;
                                if m.x == test as i32 && m.y == j as i32 {
                                    v2.push(l.clone());
                                }
                            }
                            for s in v2 {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[k][j]);
                            }
                        }
                    }
                }
            }
            Dir::Down => {
                for j in 0..=3 {
                    for i in (1..=3).rev() {
                        let mut next_row: usize = i - 1;
                        let mut of = false;
                        while !self.arr[next_row][j].exists {
                            if next_row != 0 {
                                next_row -= 1;
                            } else {
                                of = true;
                                break;
                            }
                        }
                        if of {
                            break;
                        }

                        let mut test = next_row;

                        if i == 3 && !self.arr[i][j].exists {
                            next_row = i;
                            for k in (0..=next_row).rev() {
                                while !self.arr[test][j].exists {
                                    if test != 0 {
                                        test -= 1;
                                    } else {
                                        of = true;
                                        break;
                                    }
                                }
                                if of {
                                    break;
                                }
                                self.arr[k][j].value = self.arr[test][j].value;
                                self.arr[k][j].exists = true;
                                self.arr[test][j].exists = false;

                                let mut v2 = Vec::new();
                                for k in self.labels.iter() {
                                    let (l, m) = k;
                                    if m.x == test as i32 && m.y == j as i32 {
                                        v2.push(l.clone());
                                    }
                                }
                                for s in v2 {
                                    self.labels.remove(&s);
                                    self.labels.insert(s, self.arr[k][j]);
                                }
                            }
                            next_row = 2;
                            of = false;
                            while !self.arr[next_row][j].exists {
                                if next_row != 0 {
                                    next_row -= 1;
                                } else {
                                    of = true;
                                    break;
                                }
                            }
                            if of {
                                break;
                            }
                        }
                        //Add the next tile
                        if self.arr[i][j].exists
                            && self.arr[i][j].value == self.arr[next_row][j].value
                        {
                            self.arr[i][j].value /= self.arr[next_row][j].value;
                            self.arr[next_row][j].exists = false;

                            let mut v = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if m.x == next_row as i32 && m.y == j as i32 {
                                    v.push(l.clone());
                                }
                            }

                            for s in v {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[i][j]);
                            }
                        } else if self.arr[i][j].exists {
                            continue;
                        } else {
                            next_row = i;
                        }
                        of = false;
                        //Compress
                        for k in (0..=next_row).rev() {
                            while !self.arr[test][j].exists {
                                if test != 0 {
                                    test -= 1;
                                } else {
                                    of = true;
                                    break;
                                }
                            }
                            if of {
                                break;
                            }
                            self.arr[k][j].value = self.arr[test][j].value;
                            self.arr[k][j].exists = true;
                            self.arr[test][j].exists = false;

                            let mut v2 = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if m.x == test as i32 && m.y == j as i32 {
                                    v2.push(l.clone());
                                }
                            }
                            for s in v2 {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[k][j]);
                            }
                        }
                    }
                }
            }
            Dir::Left => {
                for i in 0..=3 {
                    for j in 0..=2 {
                        let mut next_col = j + 1;
                        while next_col < 4 && !self.arr[i][next_col].exists {
                            next_col += 1;
                        }
                        if next_col == 4 {
                            break;
                        }

                        let mut test = next_col;

                        if j == 0 && !self.arr[i][j].exists {
                            next_col = j;
                            for k in next_col..4 {
                                while test < 4 && !self.arr[i][test].exists {
                                    test += 1;
                                }
                                if test == 4 {
                                    break;
                                }
                                self.arr[i][k].value = self.arr[i][test].value;
                                self.arr[i][k].exists = true;
                                self.arr[i][test].exists = false;
                                let mut v2 = Vec::new();
                                for t in self.labels.iter() {
                                    let (l, m) = t;
                                    if m.x == i as i32 && m.y == test as i32 {
                                        v2.push(l.clone());
                                    }
                                }
                                for s in v2 {
                                    self.labels.remove(&s);
                                    self.labels.insert(s, self.arr[i][k]);
                                }
                            }
                            next_col = 1;
                            while next_col < 4 && !self.arr[i][next_col].exists {
                                next_col += 1;
                            }
                            if next_col == 4 {
                                break;
                            }
                        }
                        //Add the next tile
                        if self.arr[i][j].exists
                            && self.arr[i][j].value == self.arr[i][next_col].value
                        {
                            self.arr[i][j].value /= self.arr[i][next_col].value;
                            self.arr[i][next_col].exists = false;

                            let mut v = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if m.x == i as i32 && m.y == next_col as i32 {
                                    v.push(l.clone());
                                }
                            }

                            for s in v {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[i][j]);
                            }
                        } else if self.arr[i][j].exists {
                            continue;
                        } else {
                            next_col = j;
                        }
                        //Compress
                        for k in next_col..4 {
                            while test < 4 && !self.arr[i][test].exists {
                                test += 1;
                            }
                            if test == 4 {
                                break;
                            }
                            self.arr[i][k].value = self.arr[i][test].value;
                            self.arr[i][k].exists = true;
                            self.arr[i][test].exists = false;
                            let mut v2 = Vec::new();
                            for t in self.labels.iter() {
                                let (l, m) = t;
                                if m.x == i as i32 && m.y == test as i32 {
                                    v2.push(l.clone());
                                }
                            }
                            for s in v2 {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[i][k]);
                            }
                        }
                    }
                }
            }
            Dir::Right => {
                for i in 0..=3 {
                    for j in (1..=3).rev() {
                        let mut next_col: usize = j - 1;
                        let mut of = false;
                        while !self.arr[i][next_col].exists {
                            if next_col != 0 {
                                next_col -= 1;
                            } else {
                                of = true;
                                break;
                            }
                        }
                        if of {
                            break;
                        }

                        let mut test = next_col;

                        if j == 3 && !self.arr[i][j].exists {
                            next_col = j;
                            for k in (0..=next_col).rev() {
                                while !self.arr[i][test].exists {
                                    if test != 0 {
                                        test -= 1;
                                    } else {
                                        of = true;
                                        break;
                                    }
                                }
                                if of {
                                    break;
                                }
                                self.arr[i][k].value = self.arr[i][test].value;
                                self.arr[i][k].exists = true;
                                self.arr[i][test].exists = false;

                                let mut v2 = Vec::new();
                                for k in self.labels.iter() {
                                    let (l, m) = k;
                                    if m.x == i as i32 && m.y == test as i32 {
                                        v2.push(l.clone());
                                    }
                                }
                                for s in v2 {
                                    self.labels.remove(&s);
                                    self.labels.insert(s, self.arr[i][k]);
                                }
                            }
                            next_col = 2;
                            of = false;
                            while !self.arr[i][next_col].exists {
                                if next_col != 0 {
                                    next_col -= 1;
                                } else {
                                    of = true;
                                    break;
                                }
                            }
                            if of {
                                break;
                            }
                        }

                        //Add the next tile
                        if self.arr[i][j].exists
                            && self.arr[i][j].value == self.arr[i][next_col].value
                        {
                            self.arr[i][j].value /= self.arr[i][next_col].value;
                            self.arr[i][next_col].exists = false;

                            let mut v = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if m.x == i as i32 && m.y == next_col as i32 {
                                    v.push(l.clone());
                                }
                            }

                            for s in v {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[i][j]);
                            }
                        } else if self.arr[i][j].exists {
                            continue;
                        } else {
                            next_col = j;
                        }
                        of = false;
                        //Compress
                        for k in (0..=next_col).rev() {
                            while !self.arr[i][test].exists {
                                if test != 0 {
                                    test -= 1;
                                } else {
                                    of = true;
                                    break;
                                }
                            }
                            if of {
                                break;
                            }
                            self.arr[i][k].value = self.arr[i][test].value;
                            self.arr[i][k].exists = true;
                            self.arr[i][test].exists = false;

                            let mut v2 = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if m.x == i as i32 && m.y == test as i32 {
                                    v2.push(l.clone());
                                }
                            }
                            for s in v2 {
                                self.labels.remove(&s);
                                self.labels.insert(s, self.arr[i][k]);
                            }
                        }
                    }
                }
            }
        }
    }

    fn assign(&mut self, value: Box<AstNode>, loca: (i32, i32)) {
        let _val = *value;
        let (mut pos1, mut pos2) = loca;
        pos1 -= 1;
        pos2 -= 1;
        match _val {
            AstNode::Val { val } => {
                if val == 0 && self.arr[pos1 as usize][pos2 as usize].exists {
                    self.arr[pos1 as usize][pos2 as usize].exists = false;

                    let mut v = Vec::new();
                    for k in self.labels.iter() {
                        let (l, m) = k;
                        if m.x == pos1 as i32 && m.y == pos2 as i32 {
                            v.push(l.clone());
                        }
                    }
                    for s in v {
                        self.labels.remove(&s);
                    }
                } else if val == 0 {
                    self.arr[pos1 as usize][pos2 as usize] = Tile {
                        x: pos1,
                        y: pos2,
                        value: val,
                        exists: false,
                        merged_from: None,
                    };
                } else {
                    self.arr[pos1 as usize][pos2 as usize] = Tile {
                        x: pos1,
                        y: pos2,
                        value: val,
                        exists: true,
                        merged_from: None,
                    };
                }
                self.print();
            }
            AstNode::Query { loc } => {
                let r = self.query(loc);
                match r {
                    Some(v) => {
                        if v == 0 && self.arr[pos1 as usize][pos2 as usize].exists {
                            self.arr[pos1 as usize][pos2 as usize].exists = false;
                            let mut v = Vec::new();
                            for k in self.labels.iter() {
                                let (l, m) = k;
                                if m.x == pos1 as i32 && m.y == pos2 as i32 {
                                    v.push(l.clone());
                                }
                            }
                            for s in v {
                                self.labels.remove(&s);
                            }
                        } else if v == 0 {
                            self.arr[pos1 as usize][pos2 as usize] = Tile {
                                x: pos1,
                                y: pos2,
                                value: v,
                                exists: false,
                                merged_from: None,
                            };
                        } else {
                            self.arr[pos1 as usize][pos2 as usize] = Tile {
                                x: pos1,
                                y: pos2,
                                value: v,
                                exists: true,
                                merged_from: None,
                            };
                        }
                        self.print();
                    }
                    None => {
                        println!("Assignment failed.");
                    }
                }
            }
            _ => panic!("Error"),
        }
    }

    fn name(&mut self, id: String, loc: (i32, i32)) {
        if self.labels.contains_key(&id) {
            println!("{}{}Semantic Error:{}{} {}Double assignment of a variable.{}",
            color::Fg(color::LightRed),
            style::Bold,
            style::Reset,
            color::Fg(color::Reset),
            color::Fg(color::LightWhite),
            color::Fg(color::Reset)
        );
            eprintln!("-1");
        } else {
            let (mut pos1, mut pos2) = loc;
            pos1 -= 1;
            pos2 -= 1;
            self.labels
                .insert(id, self.arr[pos1 as usize][pos2 as usize]);
            self.print();
        }
    }

    fn query(&self, loc: (i32, i32)) -> Option<i32> {
        let (mut pos1, mut pos2) = loc;
        pos1 -= 1;
        pos2 -= 1;

        if self.arr[pos1 as usize][pos2 as usize].exists {
            println!(
                "{}Value of tile in the location {}{:?}{} is: {}{}{}",
                color::Fg(color::LightWhite),
                color::Fg(color::LightMagenta),
                loc,
                color::Fg(color::LightWhite),
                color::Fg(color::LightMagenta),
                self.arr[pos1 as usize][pos2 as usize].value,
                color::Fg(color::Reset),

            );
            Some(self.arr[pos1 as usize][pos2 as usize].value)
        } else {
            println!("{}{}Semantic Error:{}{} {}No Tile in given location.{}",
            color::Fg(color::LightRed),
            style::Bold,
            style::Reset,
            color::Fg(color::Reset),
            color::Fg(color::LightWhite),
            color::Fg(color::Reset));
            eprintln!("-1");
            None
        }
    }

    pub fn eval_ast(&mut self, ast: Vec<AstNode>) {
        for i in ast {
            match i {
                AstNode::Node(x) => {
                    let v = vec![*x];
                    self.eval_ast(v);
                }
                AstNode::Move { op, dir } => self.mov(op, dir),
                AstNode::Name { id, loc } => self.name(String::from(id), loc),
                AstNode::Assign { val, loc } => self.assign(val, loc),
                AstNode::Query { loc } => {
                    let _r = self.query(loc);
                }
                AstNode::Val { val: _ } => {}
            }
        }
    }
}
