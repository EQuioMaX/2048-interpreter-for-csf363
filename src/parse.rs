extern crate pest;
extern crate rustyline;

use error::error_handler;
use pest::{error::Error, iterators::Pair, Parser};

use crate::error;

#[cfg(debug_assertions)]
const _GRAMMAR: &str = include_str!("grammar.pest");

#[derive(Parser)]
#[grammar = "grammar.pest"]
pub struct SimParser;

#[derive(PartialEq, Debug, Clone)]
pub enum AstNode<'a> {
    Node(Box<AstNode<'a>>),
    Move {
        op: Op,
        dir: Dir,
    },
    Name {
        id: &'a str,
        loc: (i32, i32),
    },
    Assign {
        val: Box<AstNode<'a>>,
        loc: (i32, i32),
    },
    Query {
        loc: (i32, i32),
    },
    Val {
        val: i32,
    },
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum Op {
    Add,
    Subtract,
    Multiply,
    Divide,
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum Dir {
    Up,
    Down,
    Left,
    Right,
}

fn build_ast(pair: Pair<Rule>) -> AstNode {
    match pair.as_rule() {
        Rule::inp => build_ast(pair.into_inner().next().unwrap()),

        Rule::expr => build_ast(pair.into_inner().next().unwrap()),

        Rule::mov => {
            let mut pair = pair.into_inner();
            let _op = pair.next().unwrap().as_str();
            let _dir = pair.next().unwrap().as_str();

            //println!("Parsed: {} {}", _op, _dir);
            AstNode::Move {
                op: match _op {
                    "ADD" => Op::Add,
                    "SUBTRACT" => Op::Subtract,
                    "MULTIPLY" => Op::Multiply,
                    "DIVIDE" => Op::Divide,
                    _ => {
                        panic!("ERROR");
                    }
                },
                dir: match _dir {
                    "UP" => Dir::Up,
                    "DOWN" => Dir::Down,
                    "LEFT" => Dir::Left,
                    "RIGHT" => Dir::Right,
                    _ => {
                        panic!("ERROR");
                    }
                },
            }
        }

        Rule::name => {
            let mut pair = pair.into_inner();
            let _id = pair.next().unwrap().as_str();
            let _loc = pair.next().unwrap().as_str();
            let tup: (i32, i32) = tup_from_str(_loc);

            //println!("Parsed: {} {}", _id, _loc);
            AstNode::Name { id: _id, loc: tup }
        }

        Rule::query => {
            let mut pair = pair.into_inner();
            let _loc = pair.next().unwrap().as_str();
            let tup: (i32, i32) = tup_from_str(_loc);

            //println!("Parsed: {}", _loc);
            AstNode::Query { loc: tup }
        }

        Rule::assignment => {
            let mut pair = pair.into_inner();
            let _val = pair.next().unwrap().as_str();
            let _loc = pair.next().unwrap().as_str();

            //println!("Parsed: {} {}", _val, _loc);
            let tup: (i32, i32) = tup_from_str(_loc);
            let test = _val.parse();

            match test {
                Ok(v) => AstNode::Assign {
                    val: Box::new(AstNode::Val { val: v }),
                    loc: tup,
                },

                Err(_e) => {
                    println!("Huehue: {:?}", _e);
                    let test = parse_expr(_val, Rule::expr);
                    match test {
                        Ok(v) => AstNode::Assign {
                            val: Box::new(v[0].clone()),
                            loc: tup,
                        },
                        Err(e) => {
                            error_handler(e);
                            panic!();
                        }
                    }
                }
            }
        }

        Rule::help => {
            super::print_help();
            AstNode::Val { val: 0 }
        }

        _ => {
            panic!("ERROR");
        }
    }
}

pub fn parse_expr(s: &str, r: Rule) -> Result<Vec<AstNode>, Error<Rule>> {
    let mut ast = vec![];
    let pairs = SimParser::parse(r, s)?;

    for pair in pairs {
        match pair.as_rule() {
            Rule::inp => {
                ast.push(AstNode::Node(Box::new(build_ast(pair))));
            }
            Rule::expr => {
                let expr = build_ast(pair);
                ast.push(expr);
            }
            _ => {
                
            }
        }
    }
    //println!("AST: {:?}", ast);
    Ok(ast)
}


fn tup_from_str(s: &str) -> (i32, i32) {
    let coords: Vec<&str> = s
        .trim_matches(|p| p == '(' || p == ')')
        .split(',')
        .collect();

    let x_fromstr = coords[0].parse::<i32>().unwrap();
    let y_fromstr = coords[1].parse::<i32>().unwrap();

    (x_fromstr, y_fromstr)
}
